import React, {Component} from 'react';
import Modal from "./Modal";

/**
 * Represents a modal for deleting one selected note.
 */
class DeleteNoteModal extends Component {

  constructor(props) {
    super(props);
    this.yesBtnRef = React.createRef();
    this.noBtnRef = React.createRef();
  }

  /**
   * Gets elements needed to use for different actions by their ID.
   * Appends "click" listeners to buttons.
   */
  componentDidMount() {
    this.deleteNoteModalRef = document.querySelector("#deleteNoteModal");
    this.main = document.querySelector("#main");
    this.header = document.querySelector("#header");
    this.yesBtnRef.addEventListener("click", () => {this.deleteNote();});
    this.noBtnRef.addEventListener("click", () => {this.closeDeleteNoteModal();});
  }

  /**
   * Deletes the selected note.
   */
  deleteNote() {
    let noteId = this.deleteNoteModalRef.getAttribute("data-note-id");
    this.props.app.slideSoundRef.play();
    let note = document.querySelector(`#${noteId}`);
    note.style.maxHeight = note.offsetHeight + "px";
    setTimeout(() => {note.classList.add("deleted");}, 0); // Force DOM redraw
    setTimeout(() => {this.props.app.deleteNote(noteId)}, 400);
    this.closeDeleteNoteModal();
  }

  /**
   * Closes this modal.
   */
  closeDeleteNoteModal() {
    this.main.classList.remove("blurred");
    this.header.classList.remove("blurred");
    this.deleteNoteModalRef.classList.add("transparent");
    setTimeout(() => {this.deleteNoteModalRef.classList.add("hidden");}, 200);
  }

  /**
   * Renders this modal.
   * @returns {*}
   */
  render() {
    return (
      <Modal _id="deleteNoteModal"
             _content={
               <section>
                  <h2>Smazat poznámku</h2>
                  <p>Skutečně chcete smazat tuto poznámku?</p>
                  <button ref={el => this.yesBtnRef = el} type="button" className="redBtn">Ano</button>
                  <button ref={el => this.noBtnRef = el} type="button" className="greenBtn">Ne</button>
               </section>
             }
      />
    );
  }
}

export default DeleteNoteModal;
