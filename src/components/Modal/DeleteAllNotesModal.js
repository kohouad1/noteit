import React, {Component} from 'react';
import Modal from "./Modal";

/**
 * Represents a modal for deleting all notes.
 */
class DeleteAllNotesModal extends Component {

  constructor(props) {
    super(props);
    this.yesBtnRef = React.createRef();
    this.noBtnRef = React.createRef();
  }

  /**
   * Gets elements needed to use for different actions by their ID.
   * Appends "click" listeners to buttons.
   * Adds state pushing when a particular button is clicked.
   * Appends a "popstate" listener, i.e. when the user uses backward/forward buttons to move through history.
   */
  componentDidMount() {
    this.deleteAllNotesModalRef = document.querySelector("#deleteAllNotesModal");
    this.main = document.querySelector("#main");
    this.header = document.querySelector("#header");
    this.yesBtnRef.addEventListener("click", () => {
      window.history.pushState(null, null, "/");
      this.deleteAllNotes();
    });
    this.noBtnRef.addEventListener("click", () => {
      window.history.pushState(null, null, "/");
      this.closeDeleteAllNotesModal();
    });
    window.addEventListener("popstate", (e) => {
      if (!this.deleteAllNotesModalRef.classList.contains("hidden") && !e.state)
        this.closeDeleteAllNotesModal();
    })
  }

  /**
   * Deletes all saved notes.
   */
  deleteAllNotes() {
    let notes = document.querySelectorAll("#noteList > .listItem");
    for (let i = 0; i < notes.length; i++) {
      notes[i].style.maxHeight = notes[i].offsetHeight + "px";
      setTimeout(() => {notes[i].classList.add("deleted");}, 0); // Force DOM redraw
    }
    setTimeout(() => {this.props.app.deleteAllNotes();}, 400);
    this.props.app.slideSoundRef.play();
    this.closeDeleteAllNotesModal();
  }

  /**
   * Closes this modal.
   */
  closeDeleteAllNotesModal() {
    this.main.classList.remove("blurred");
    this.header.classList.remove("blurred");
    this.deleteAllNotesModalRef.classList.add("transparent");
    setTimeout(() => {this.deleteAllNotesModalRef.classList.add("hidden");}, 200);
  }

  /**
   * Renders this modal.
   * @returns {*}
   */
  render() {
    return (
      <Modal _id="deleteAllNotesModal"
             _content={
               <section>
                 <h2>Smazat všechny poznámky</h2>
                 <p>Skutečně chcete smazat všechny poznámky?</p>
                 <button ref={el => this.yesBtnRef = el} type="button" className="redBtn">Ano</button>
                 <button ref={el => this.noBtnRef = el} type="button" className="greenBtn">Ne</button>
               </section>
             }
      />
    );
  }
}

export default DeleteAllNotesModal;
