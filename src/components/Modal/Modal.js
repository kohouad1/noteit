import React, {Component} from 'react';
import "./Modal.css";

/**
 * Represents a generic modal that accepts a content to be rendered.
 */
class Modal extends Component {
  /**
   * Renders generic modal with the provided content.
   * @returns {*}
   */
  render() {
    return (
      <div id={this.props._id} data-note-id="" className="modal hidden transparent">
        {this.props._content}
      </div>
    )
  }
}

export default Modal;
