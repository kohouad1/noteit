import React, {Component} from 'react';
import Modal from "./Modal";
import CreateNoteFromFileForm from "./Forms/CreateNoteFromFileForm";

/**
 * Represents a modal containing a form for creating a new note from file.
 */
class CreateNoteFromFileModal extends Component {
  /**
   * Renders this modal.
   * @returns {*}
   */
  render() {
    return (
      <Modal _id="createNoteFromFileModal"
             _content={<CreateNoteFromFileForm addNote={this.props.app.addNote.bind(this.props.app)}
                                               _coords={this.props.app.state._coords}
                                               clickSoundRef={this.props.app.clickSoundRef} />

             }

      />
    );
  }

}

export default CreateNoteFromFileModal;
