/**
 * Helper function used for a note name validation.
 * @param noteName a note name to validate
 * @returns {string|null} an error string if a validation fails; <code>null</code> otherwise
 */
export function validateNoteName(noteName) {
  if (noteName.length < 1 || noteName.replace(/ +?/g, '').length < 1)
    return "Název poznámky nesmí být prázdný.";
  if (noteName.length > 100)
    return "Název poznámky může mít maximálně 100 znaků.";
  return null;
}

/**
 * Helper function used for a note text validation.
 * @param noteText a note text to validate
 * @returns {string|null} an error string if a validation fails; <code>null</code> otherwise
 */
export function validateNoteText(noteText) {
  if (noteText.length < 1 || noteText.replace(/ +?/g, '').length < 1)
    return "Text poznámky nesmí být prázdný.";
  if (noteText.length > 5000)
    return "Text poznámky může mít maximálně 5000 znaků.";
  return null;
}
