import React, {Component} from 'react';
import COLORS, {getColorTypeFromValue} from "../../../Colors";
import Note from "../../../Note";
import {validateNoteName, validateNoteText} from "./Validation";

/**
 * Represents a form for creating a new note that is rendered in its modal.
 */
class CreateNoteForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
      selectedColor: COLORS.RED.value,
      noteNameError: null,
      noteTextError: null
    };
    this.noteNameFieldRef = React.createRef();
    this.noteTextFieldRef = React.createRef();
    this.createNoteBtnRef = React.createRef();
    this.closeBtnRef = React.createRef();
    this.form = React.createRef();
  }

  /**
   * Gets elements needed to use for different actions by their ID.
   * Appends "click" listeners to buttons.
   * Adds state pushing when a particular button is clicked.
   * Appends a "popstate" listener, i.e. when the user uses backward/forward buttons to move through history.
   */
  componentDidMount() {
    this.main = document.querySelector("#main");
    this.header = document.querySelector("#header");
    this.createNoteModal = document.querySelector("#createNoteModal");
    this.createNoteBtnRef.addEventListener("click", () => {
      this.validateForm();
    });
    this.closeBtnRef.addEventListener("click", () => {
      window.history.pushState(null, null, "/");
      this.closeCreateNoteForm();
    });
    window.addEventListener("popstate", (e) => {
      if (!this.createNoteModal.classList.contains("hidden") && !e.state)
        this.closeCreateNoteForm();
    });
    this.form.addEventListener("submit", (e) => e.preventDefault());
  }

  /**
   * Creates a new note and closes this form.
   */
  createNote() {
    const note = new Note(
      CreateNoteForm.generateUniqueID(),
      this.noteNameFieldRef.value,
      this.noteTextFieldRef.value,
      getColorTypeFromValue(this.state.selectedColor),
      new Date(),
      this.props._coords
    );
    this.props.addNote(note);
    this.props.clickSoundRef.play();
    this.closeCreateNoteForm();
  }

  /**
   * Validates this form.
   */
  validateForm() {
    this.setState({
      noteNameError: validateNoteName(this.noteNameFieldRef.value),
      noteTextError: validateNoteText(this.noteTextFieldRef.value)
    });
    if (this.state.noteNameError)
      this.noteNameFieldRef.classList.add("error");
    else
      this.noteNameFieldRef.classList.remove("error");
    if (this.state.noteTextError)
      this.noteTextFieldRef.classList.add("error");
    else
      this.noteTextFieldRef.classList.remove("error");
    if (!this.state.noteNameError && !this.state.noteTextError) {
      this.createNote();
      window.history.pushState(null, null, "/");
    }
  }

  /**
   * Closes this form.
   */
  closeCreateNoteForm() {
    this.main.classList.remove("blurred");
    this.header.classList.remove("blurred");
    this.createNoteModal.classList.add("transparent");
    setTimeout(() => {this.createNoteModal.classList.add("hidden")}, 200);
  }

  /**
   * Generates a unique note ID based on current time.
   * @returns {string} unique note ID
   */
  static generateUniqueID() {
    return `note${new Date().getTime()}`;
  }

  /**
   * Changes state of this form based on a selected color.
   * @param e event object
   */
  handleColorSelectionChange(e) {
    this.setState({
      selectedColor: e.target.value
    });
  }

  /**
   * Renders this form.
   * @returns {*}
   */
  render() {
    return (
      <section id="createNoteForm">
        <h2>Vytvořit poznámku</h2>
        <form ref={el => this.form = el}>
          <h3>Název a text poznámky</h3>
          <label htmlFor="noteName" className="hidden">Název poznámky</label>
          <input ref={el => this.noteNameFieldRef = el} type="text" name="noteName" id="noteName" placeholder="Název poznámky" required={"required"} maxLength={100} />
          {this.state.noteNameError ? (<label htmlFor="noteName" className="error">{this.state.noteNameError}</label>) : ("")}
          <label htmlFor="noteText" className="hidden">Text poznámky</label>
          <textarea ref={el => this.noteTextFieldRef = el} name="noteText" id="noteText" cols="30" rows="5" placeholder="Text poznámky" required={"required"} maxLength={5000}></textarea>
          {this.state.noteTextError ? (<label htmlFor="noteText" className="error">{this.state.noteTextError}</label>) : ("")}
          <h3>Barva poznámky</h3>
          {
            Object.keys(COLORS).map((key, index) => {
              return [
                <input
                  key="noteColorInputKey"
                  type="radio"
                  name="noteColor"
                  value={COLORS[key].value}
                  id={COLORS[key].value}
                  checked={this.state.selectedColor === COLORS[key].value}
                  onChange={this.handleColorSelectionChange.bind(this)} />,
                <div key="colorPreviewKey" className="colorPreview" style={{backgroundColor: COLORS[key].value}}></div>,
                <label key="noteColorInputLabelKey" htmlFor={COLORS[key].value}>{COLORS[key].name}</label>,
                <br key="br" />
              ];
            })
          }
          <button ref={el => this.createNoteBtnRef = el} type="submit" id="createNoteBtn" className="greenBtn">Vytvořit poznámku</button><br />
          <button ref={el => this.closeBtnRef = el} type="button" id="closeCreateNoteFormBtn" className="redBtn">Zavřít</button>
        </form>
      </section>
    );
  }
}

export default CreateNoteForm;
