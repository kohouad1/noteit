import React, {Component} from 'react';
import COLORS, {getColorTypeFromValue} from "../../../Colors";
import Note from "../../../Note";
import {validateNoteName} from "./Validation";

/**
 * Represents a form for creating a new note from file that is rendered in its modal.
 */
class CreateNoteFromFileForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
      selectedColor: COLORS.RED.value,
      noteNameError: null,
      fileInputError: null
    };
    this.noteNameFieldRef = React.createRef();
    this.fileInput = React.createRef();
    this.createNoteFromFileBtnRef = React.createRef();
    this.closeBtnRef = React.createRef();
    this.form = React.createRef();
  }

  /**
   * Gets elements needed to use for different actions by their ID.
   * Appends "click" listeners to buttons.
   * Adds state pushing when a particular button is clicked.
   * Appends a "popstate" listener, i.e. when the user uses backward/forward buttons to move through history.
   */
  componentDidMount() {
    this.main = document.querySelector("#main");
    this.header = document.querySelector("#header");
    this.createNoteFromFileModal = document.querySelector("#createNoteFromFileModal");
    this.createNoteFromFileBtnRef.addEventListener("click", () => {
      this.validateForm();
    });
    this.closeBtnRef.addEventListener("click", () => {
      window.history.pushState(null, null, "/");
      this.closeCreateNoteFromFileForm();
    });
    window.addEventListener("popstate", (e) => {
      if (!this.createNoteFromFileModal.classList.contains("hidden") && !e.state)
        this.closeCreateNoteFromFileForm();
    });
    this.form.addEventListener("submit", (e) => e.preventDefault());
  }

  /**
   * Creates a new note from file and closes this form.
   * @param fileData file contents that were read from the uploaded file
   */
  createNoteFromFile(fileData) {
    const note = new Note(
      CreateNoteFromFileForm.generateUniqueID(),
      this.noteNameFieldRef.value,
      fileData,
      getColorTypeFromValue(this.state.selectedColor),
      new Date(),
      this.props._coords
    );
    this.props.addNote(note);
    this.props.clickSoundRef.play();
    this.closeCreateNoteFromFileForm();
  }

  /**
   * Validates this form.
   */
  validateForm() {
    this.setState({ noteNameError: validateNoteName(this.noteNameFieldRef.value)});
    if (this.state.noteNameError)
      this.noteNameFieldRef.classList.add("error");
    else
      this.noteNameFieldRef.classList.remove("error");
    this.validateFile(this.fileInput);
  }

  /**
   * Validates the uploaded file.
   * @param fileInputElement file input element to validate the uploaded file from
   */
  validateFile(fileInputElement) {
    if (fileInputElement.files.length < 1) {
      this.setState({ fileInputError: "Nahrajte prosím soubor."});
    } else {
      if (!fileInputElement.files[0].type.match("text/plain")) {
        this.setState({ fileInputError: "Nesprávný formát souboru. Povoleným formátem je textový soubor."});
      } else if (fileInputElement.files[0].size > 5120)
        this.setState({ fileInputError: "Soubor je příliš velký. Maximální povolená velikost souboru je 5 kB."});
      else {
        this.setState({ fileInputError: null });
        if (!this.state.noteNameError) {
          this.processFile(fileInputElement.files[0]);
          window.history.pushState(null, null, "/");
        }
      }
    }
  }

  /**
   * Reads the uploaded file.
   * @param file a file to read
   */
  processFile(file) {
    this.setState({ fileInputError: null });
    let reader = new FileReader();
    reader.addEventListener("load", () => {
      this.createNoteFromFile(reader.result);
    });
    reader.readAsText(file);
  }

  /**
   * Closes this form.
   */
  closeCreateNoteFromFileForm() {
    this.main.classList.remove("blurred");
    this.header.classList.remove("blurred");
    this.createNoteFromFileModal.classList.add("transparent");
    setTimeout(() => {this.createNoteFromFileModal.classList.add("hidden")}, 200)
  }

  /**
   * Generates a unique note ID based on current time.
   * @returns {string} unique note ID
   */
  static generateUniqueID() {
    return `note${new Date().getTime()}`;
  }

  /**
   * Changes state of this form based on a selected color.
   * @param e event object
   */
  handleColorSelectionChange(e) {
    this.setState({
      selectedColor: e.target.value
    });
  }

  /**
   * Renders this form.
   * @returns {*}
   */
  render() {
    return (
      <section id="createNoteFromFileForm">
        <h2>Vytvořit novou poznámku ze souboru</h2>
        <form ref={el => this.form = el}>
          <h3>Název poznámky</h3>
          <label htmlFor="noteFromFileName" className="hidden">Název poznámky</label>
          <input ref={el => this.noteNameFieldRef = el} type="text" name="noteFromFileName" id="noteFromFileName" placeholder="Název poznámky" required={"required"} maxLength={100} />
          {this.state.noteNameError ? (<label htmlFor="noteFromFileName" className="error">{this.state.noteNameError}</label>) : ("")}
          <h3>Text poznámky</h3>
          <p>Vyberte textový soubor pro vložení textu poznámky.</p>
          <label htmlFor="fileInput" className="hidden">Soubor</label>
          <input ref={el => this.fileInput = el} type="file" name="fileInput" id="fileInput" accept=".txt" required={"required"} /><br />
          {this.state.fileInputError ? (<label htmlFor="fileInput" className="error">{this.state.fileInputError}</label>) : ("")}
          <h3>Barva poznámky</h3>
          {
            Object.keys(COLORS).map((key, index) => {
              return [
                <input
                  key="noteColorInputKey"
                  type="radio"
                  name="noteFromFileColor"
                  value={COLORS[key].value}
                  id={"fileInput" + COLORS[key].value}
                  checked={this.state.selectedColor === COLORS[key].value}
                  onChange={this.handleColorSelectionChange.bind(this)} />,
                <div key="colorPreviewKey" className="colorPreview" style={{backgroundColor: COLORS[key].value}}></div>,
                <label key="noteColorInputLabelKey" htmlFor={"fileInput" + COLORS[key].value}>{COLORS[key].name}</label>,
                <br key="br" />
              ];
            })
          }
          <button ref={el => this.createNoteFromFileBtnRef = el} type="submit" id="createNoteFromFileBtn" className="greenBtn">Vytvořit poznámku ze souboru</button>
          <button ref={el => this.closeBtnRef = el} type="button" id="closeCreateFromFileFormBtn" className="redBtn">Zavřít</button>
        </form>
      </section>
    )
  }
}

export default CreateNoteFromFileForm;
