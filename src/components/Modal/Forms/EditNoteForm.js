import React, {Component} from 'react';
import COLORS, {getColorTypeFromValue} from "../../../Colors";
import Note from "../../../Note";
import {validateNoteName, validateNoteText} from "./Validation";

/**
 * Represents a form for editing a selected note that is rendered in its modal.
 */
class EditNoteForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
      selectedColor: COLORS.RED.value,
      noteNameError: null,
      noteTextError: null
    };
    this.noteNameFieldRef = React.createRef();
    this.noteTextFieldRef = React.createRef();
    this.updateNoteBtnRef = React.createRef();
    this.closeBtnRef = React.createRef();
    this.form = React.createRef();
  }

  /**
   * Gets elements needed to use for different actions by their ID.
   * Appends "click" listeners to buttons.
   */
  componentDidMount() {
    this.main = document.querySelector("#main");
    this.header = document.querySelector("#header");
    this.editNoteModal = document.querySelector("#editNoteModal");
    this.updateNoteBtnRef.addEventListener("click", () => this.validateForm());
    this.closeBtnRef.addEventListener("click", () => this.closeEditNoteForm());
    this.form.addEventListener("submit", (e) => e.preventDefault());
  }

  /**
   * Updates the edited note and closes this form.
   */
  updateNote() {
    const note = new Note(
      this.editNoteModal.getAttribute("data-note-id"),
      this.noteNameFieldRef.value,
      this.noteTextFieldRef.value,
      getColorTypeFromValue(this.state.selectedColor),
      null,
      null
    );
    this.props.editNote(note);
    this.props.clickSoundRef.play();
    this.closeEditNoteForm();
  }

  /**
   * Validates this form.
   */
  validateForm() {
    this.setState({
      noteNameError: validateNoteName(this.noteNameFieldRef.value),
      noteTextError: validateNoteText(this.noteTextFieldRef.value)
    });
    if (this.state.noteNameError)
      this.noteNameFieldRef.classList.add("error");
    else
      this.noteNameFieldRef.classList.remove("error");
    if (this.state.noteTextError)
      this.noteTextFieldRef.classList.add("error");
    else
      this.noteTextFieldRef.classList.remove("error");
    if (!this.state.noteNameError && !this.state.noteTextError)
      this.updateNote();
  }

  /**
   * Closes this form.
   */
  closeEditNoteForm() {
    this.main.classList.remove("blurred");
    this.header.classList.remove("blurred");
    this.editNoteModal.classList.add("transparent");
    setTimeout(() => {this.editNoteModal.classList.add("hidden")}, 200);
  }

  /**
   * Changes state of this form based on a selected color.
   * @param e event object
   */
  handleColorSelectionChange(e) {
    this.setState({
      selectedColor: e.target.value
    })
  }

  /**
   * Renders this form.
   * @returns {*}
   */
  render() {
    return (
      <section id="editNoteForm">
        <h2>Upravit poznámku</h2>
        <form ref={el => this.form = el}>
          <h3>Název a text poznámky</h3>
          <label htmlFor="editNoteName" className="hidden">Název poznámky</label>
          <input ref={el => this.noteNameFieldRef = el} type="text" name="editNoteName" id="editNoteName" placeholder="Název poznámky" required={"required"} maxLength={100} />
          {this.state.noteNameError ? (<label htmlFor="editNoteName" className="error">{this.state.noteNameError}</label>) : ("")}
          <label htmlFor="editNoteText" className="hidden">Text poznámky</label>
          <textarea ref={el => this.noteTextFieldRef = el} name="editNoteText" id="editNoteText" cols="30" rows="5" placeholder="Text poznámky" required={"required"} maxLength={5000} ></textarea>
          {this.state.noteTextError ? (<label htmlFor="editNoteText" className="error">{this.state.noteTextError}</label>) : ("")}
          <h3>Barva poznámky</h3>
          {
            Object.keys(COLORS).map((key, index) => {
              return [
                <input
                  key="noteColorInputKey"
                  type="radio"
                  name="editNoteColor"
                  value={COLORS[key].value}
                  id={"editNote" + COLORS[key].value}
                  checked={this.state.selectedColor === COLORS[key].value}
                  onChange={this.handleColorSelectionChange.bind(this)} />,
                <div key="colorPreviewKey" className="colorPreview" style={{backgroundColor: COLORS[key].value}}></div>,
                <label key="noteColorInputLabelKey" htmlFor={"editNote" + COLORS[key].value}>{COLORS[key].name}</label>,
                <br key="br" />
              ];
            })
          }
          <button ref={el => this.updateNoteBtnRef = el} type="submit" id="editNoteBtn" className="greenBtn">Potvrdit změny</button><br />
          <button ref={el => this.closeBtnRef = el} type="button" id="closeEditNoteFormBtn" className="redBtn">Zavřít</button>
        </form>
      </section>
    );
  }
}

export default EditNoteForm;
