import React, {Component} from 'react';
import Modal from "./Modal";
import CreateNoteForm from "./Forms/CreateNoteForm";

/**
 * Represents a modal containing a form for creating a new note.
 */
class CreateNoteModal extends Component {
  /**
   * Renders this modal.
   * @returns {*}
   */
  render() {
    return (
      <Modal _id="createNoteModal"
             _content={<CreateNoteForm addNote={this.props.app.addNote.bind(this.props.app)}
                                       _coords={this.props.app.state._coords}
                                       clickSoundRef={this.props.app.clickSoundRef} />
             }
      />
    );
  }
}

export default CreateNoteModal;
