import React, {Component} from 'react';
import Modal from "./Modal";
import EditNoteForm from "./Forms/EditNoteForm";

/**
 * Represents a modal containing a form for editing a selected note.
 */
class EditNoteModal extends Component {

  constructor(props) {
    super(props);
    this.editNoteFormRef = React.createRef();
  }

  /**
   * Renders this modal.
   * @returns {*}
   */
  render() {
    return (
      <Modal _id="editNoteModal"
             _content={<EditNoteForm ref="editNoteFormRef"
                                     editNote={this.props.app.editNote.bind(this.props.app)}
                                     clickSoundRef={this.props.app.clickSoundRef} />
             }
      />
    );
  }
}

export default EditNoteModal;
