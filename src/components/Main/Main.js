import React, {Component} from 'react';
import "./Main.css"
import Options from "./Options/Options";
import NoteList from "./NoteList/NoteList";

/**
 * Represents the component containing the main content,
 * i.e. the group of option buttons and the list of notes
 */
class Main extends Component {

  constructor(props) {
    super(props);
    this.mainRef = React.createRef();
  }

  /**
   * Appends a listener when the user clicks in the main content area.
   */
  componentDidMount() {
    this.mainRef.addEventListener("click", (e) => this.checkForBlurredClass(e), true)
  }

  /**
   * Checks if there is any modal opened.
   * If there is, the click event propagation is stopped here,
   * so the user cannot use clickable elements in the background.
   * @param e
   */
  checkForBlurredClass(e) {
    if (this.mainRef.classList.contains("blurred")) {
      e.stopPropagation();
    }
  }

  /**
   * Renders the component containing main content.
   * @returns {*}
   */
  render() {
    return (
      <main ref={el => this.mainRef = el} id="main">
        <Options key={"optionsKey"}
                 initialPathname={this.props.app.initialPathname}
                 updateLocation={this.props.app.updateLocation.bind(this.props.app)}
                 deleteAllNotes={this.props.app.deleteAllNotes.bind(this.props.app)}
                 slideSoundRef={this.props.app.slideSoundRef} />
        <NoteList key={"noteListKey"} app={this.props.app} />
      </main>
    );
  }
}

export default Main;
