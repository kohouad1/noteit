import React, {Component} from 'react';
import "./Options.css"

/**
 * Represents the group of buttons used for different possible actions.
 */
class Options extends Component {

  constructor(props) {
    super(props);
    this.addNewNoteBtnRef = React.createRef();
    this.addNewNoteFromFileBtnRef = React.createRef();
    this.updateLocationBtnRef = React.createRef();
    this.delAllNotesBtnRef = React.createRef();
    this.spinnerRef = React.createRef();
  }

  /**
   * Gets all the needed elements by their ID.
   * Appends "click" listeners to all buttons.
   * Adds state pushing when a particular button is clicked.
   * Appends a "popstate" listener, i.e. when the user uses backward/forward buttons to move through history.
   * Also automatically opens a given modal if there is a path name available in the URL.
   */
  componentDidMount() {
    this.createNoteModal = document.querySelector("#createNoteModal");
    this.createNoteFromFileModal = document.querySelector("#createNoteFromFileModal");
    this.deleteAllNotesModal = document.querySelector("#deleteAllNotesModal");
    this.main = document.querySelector("#main");
    this.header = document.querySelector("#header");
    this.addNewNoteBtnRef.addEventListener("click", () => {
      if (!window.history.state)
        window.history.pushState({action: "createNote"}, null, "/createNote");
      this.openCreateNoteFormModal();
    });
    this.addNewNoteFromFileBtnRef.addEventListener("click", () => {
      if (!window.history.state)
        window.history.pushState({action: "createNoteFromFile"}, null, "/createNoteFromFile");
      this.openCreateNoteFromFileModal();
    });
    this.updateLocationBtnRef.addEventListener("click", () => {
      this.spinnerRef.classList.add("show");
      this.props.updateLocation();
    });
    this.delAllNotesBtnRef.addEventListener("click", () => {
      if (!window.history.state)
        window.history.pushState({action: "deleteAllNotes"}, null, "/deleteAllNotes");
      this.openDeleteAllNotesModal();
    });
    window.addEventListener("popstate", (e) => {
      if (e.state) {
        if (e.state.action === "createNote")
          this.openCreateNoteFormModal();
        else if (e.state.action === "createNoteFromFile")
          this.openCreateNoteFromFileModal();
        else if (e.state.action === "deleteAllNotes")
          this.openDeleteAllNotesModal();
      }
    });
    if (this.props.initialPathname === "/createNote")
      this.openCreateNoteFormModal();
    else if (this.props.initialPathname === "/createNoteFromFile")
      this.openCreateNoteFromFileModal();
    else if (this.props.initialPathname === "/deleteAllNotes")
      this.openDeleteAllNotesModal();
  }

  /**
   * Opens a modal when the user clicks the "Přidat novou poznámku" button.
   */
  openCreateNoteFormModal() {
    document.querySelector("#noteName").value = "";
    document.querySelector("#noteText").value = "";
    this.openModal(this.createNoteModal);
  }

  /**
   * Opens a modal when the user clicks the "Přidat novou poznámku ze souboru" button.
   */
  openCreateNoteFromFileModal() {
    document.querySelector("#noteFromFileName").value = "";
    this.openModal(this.createNoteFromFileModal);
  }

  /**
   * Opens a modal when the user clicks the "Smazat všechny poznámky" button.
   */
  openDeleteAllNotesModal() {
    this.openModal(this.deleteAllNotesModal);
  }

  /**
   * A reusable function used for opening a specified modal.
   * @param modalToOpen a modal element to open
   */
  openModal(modalToOpen) {
    modalToOpen.classList.remove("hidden", "transparent");
    this.main.classList.add("blurred");
    this.header.classList.add("blurred");
  }

  /**
   * Renders the group of buttons.
   * @returns {*[]}
   */
  render() {
    return [
      <input key="optionsToggleKey" type="checkbox" id="optionsToggle" />,
      <label key="optionsToggleLabelKey" htmlFor="optionsToggle" id="optionsToggleLabel"><i className="fas fa-chevron-down" id="chevronIcon"></i> Možnosti</label>,
      <section key="optionsKey" id="options">
        <h2>Co chcete udělat?</h2>
        <button ref={el => this.addNewNoteBtnRef = el} type="button" className="greenBtn optionBtn"><i className="fas fa-sticky-note fa-lg optionBtnIcon"></i> Přidat novou poznámku</button><br />
        <button ref={el => this.addNewNoteFromFileBtnRef = el} type="button" className="greenBtn optionBtn"><i className="fas fa-file-alt fa-lg optionBtnIcon"></i> Přidat novou poznámku ze souboru</button><br />
        <button ref={el => this.updateLocationBtnRef = el} type="button" className="blueBtn optionBtn"><i className="fas fa-map-marker-alt fa-lg optionBtnIcon"></i> Aktualizovat polohu <i ref={el => this.spinnerRef = el} id="spinner" className="fas fa-spinner"></i></button><br />
        <button ref={el => this.delAllNotesBtnRef = el} type="button" id="deleteAllNotesBtn" className="redBtn optionBtn"><i className="fas fa-trash-alt fa-lg optionBtnIcon"></i> Smazat všechny poznámky</button>
      </section>
    ];
  }
}

export default Options;
