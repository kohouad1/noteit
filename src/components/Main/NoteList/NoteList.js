import React, {Component} from 'react';
import "./NoteList.css"
import Note from "./Note/Note";

/**
 * Represents a list of notes.
 */
class NoteList extends Component {
  /**
   * Renders a list of notes.
   * If there is at least one note provided to this component,
   * these notes will be rendered using the Note component,
   * otherwise the "Nejsou tu žádné poznámky" text will be rendered.
   * @returns {*}
   */
  render() {
    let notes = this.props.app.getNotes();
    if (notes.length > 0) {
      return (
        <ul id="noteList">
          {
            this.props.app.getNotes().map(note => {
              return (
                <Note key={note._id}
                deleteNote={this.props.app.deleteNote.bind(this.props.app)}
                _id={note._id}
                _color={note._color}
                _name={note._name}
                _text={note._text}
                _date={new Date(note._date)}
                _coords={note._coords}
                slideSoundRef={this.props.app.slideSoundRef}
                editNoteModalRef={this.props.app.editNoteModalRef}
                exists={this.props.app.exists.bind(this.props.app)}
                />
              );
            })
          }
        </ul>
      );
    } else {
      return (
        <section id="emptyNoteList">
          <h3>Nejsou tu žádné poznámky.</h3>
        </section>
      );
    }
  }
}

export default NoteList;
