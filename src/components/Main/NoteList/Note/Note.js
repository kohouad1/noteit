import React, {Component} from 'react';
import "./Note.css";
import "@fortawesome/fontawesome-free/css/all.css";
import Location from "./Location";

/**
 * Represents the note component with its rendered data.
 */
class Note extends Component {

  constructor(props) {
    super(props);
    this.listItemRef = React.createRef();
    this.delBtnRef = React.createRef();
    this.editBtnRef = React.createRef();
    this.delBtnSvgRef = React.createRef();
    this.editBtnSvgRef = React.createRef();
  }

  /**
   * Gets elements needed to use for different actions by their ID.
   */
  componentDidMount() {
    this.deleteNoteModal = document.querySelector("#deleteNoteModal");
    this.editNoteModal = document.querySelector("#editNoteModal");
    this.main = document.querySelector("#main");
    this.header = document.querySelector("#header");
    this.delBtnRef.addEventListener("click",() => {this.openDeleteNoteModal();});
    this.editBtnRef.addEventListener("click", () => {this.openEditNoteModal();});
    this.delBtnRef.addEventListener("mouseenter", this.changeSvg.bind(this, this.delBtnSvgRef, "red", 0.8));
    this.delBtnRef.addEventListener("mouseleave", this.changeSvg.bind(this, this.delBtnSvgRef, "black", 1));
    this.editBtnRef.addEventListener("mouseenter", this.changeSvg.bind(this, this.editBtnSvgRef, "blue", 0.8));
    this.editBtnRef.addEventListener("mouseleave", this.changeSvg.bind(this, this.editBtnSvgRef, "black", 1));
  }

  /**
   * Opens a modal when the user tries to delete a note.
   */
  openDeleteNoteModal() {
    this.deleteNoteModal.classList.remove("hidden");
    this.deleteNoteModal.classList.remove("transparent");
    this.deleteNoteModal.setAttribute("data-note-id", this.props._id);
    this.main.classList.add("blurred");
    this.header.classList.add("blurred");
  }

  /**
   * Opens a modal when the user tries to edit a note.
   */
  openEditNoteModal() {
    document.querySelector("#editNoteName").value = this.props._name;
    document.querySelector("#editNoteText").value = this.props._text;
    document.querySelector("#editNote\\" + this.props._color.value).checked = true;
    this.props.editNoteModalRef.refs.editNoteFormRef.setState({
      selectedColor: this.props._color.value
    });
    this.editNoteModal.classList.remove("hidden");
    this.editNoteModal.classList.remove("transparent");
    this.editNoteModal.setAttribute("data-note-id", this.props._id);
    this.main.classList.add("blurred");
    this.header.classList.add("blurred");
  }

  /**
   * Manipulates an SVG element by scaling it and changing its color.
   * @param svgEl SVG element to manipulate
   * @param colorValue a color hex value to change the color to
   * @param scaleFactor a factor to scale the SVG element by
   */
  changeSvg(svgEl, colorValue, scaleFactor) {
    svgEl.style.fill = colorValue;
    svgEl.style.transform = `scale(${scaleFactor})`;
  }

  /**
   * Renders one note with its data.
   * @returns {*}
   */
  render() {
    return (
      <li ref={el => this.listItemRef = el} className="listItem" key={this.props._id} id={this.props._id} style={{backgroundColor: this.props._color.value}}>
        <section>
          <h2 className="noteName">{this.props._name}</h2>
          {this.props._text}
        </section><hr />
        <table>
          <tbody>
            <tr title="Datum vytvoření poznámky">
              <td><i className="fas fa-calendar-day info-icon"></i></td>
              <td>{this.props._date.getDate()}. {this.props._date.getMonth() + 1}. {this.props._date.getFullYear()}
              &nbsp;{this.props._date.getHours() < 10 ? "0" + this.props._date.getHours() : this.props._date.getHours()}:{this.props._date.getMinutes() < 10 ? "0" + this.props._date.getMinutes() : this.props._date.getMinutes()}</td>
            </tr>
            <tr title="Lokace, kde byla poznámka vytvořena">
              <td><i className="fas fa-map-marker-alt info-icon"></i></td>
              <td><Location _coords={this.props._coords} /></td>
            </tr>
          </tbody>
        </table><hr />
        <button ref={el => this.delBtnRef = el} className="noteBtn" title="Smazat poznámku">
          <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="trash-alt"
               className="svg-inline--fa fa-trash-alt fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg"
               viewBox="0 0 448 512">
            <path ref={el => this.delBtnSvgRef = el} fill="currentColor"
                  d="M32 464a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128H32zm272-256a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zM432 32H312l-9.4-18.7A24 24 0 0 0 281.1 0H166.8a23.72 23.72 0 0 0-21.4 13.3L136 32H16A16 16 0 0 0 0 48v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16z"></path>
          </svg>
        </button>
        <button ref={el => this.editBtnRef = el} className="noteBtn" title="Upravit poznámku">
          <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="pen"
               className="svg-inline--fa fa-pen fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg"
               viewBox="0 0 512 512">
            <path ref={el => this.editBtnSvgRef = el} fill="currentColor"
                  d="M290.74 93.24l128.02 128.02-277.99 277.99-114.14 12.6C11.35 513.54-1.56 500.62.14 485.34l12.7-114.22 277.9-277.88zm207.2-19.06l-60.11-60.11c-18.75-18.75-49.16-18.75-67.91 0l-56.55 56.55 128.02 128.02 56.55-56.55c18.75-18.76 18.75-49.16 0-67.91z"></path>
          </svg>
        </button>
      </li>
    );
  }
}

export default Note;
