import React, {Component} from 'react';
import "./Location.css"

/**
 * Represents the location component.
 */
class Location extends Component {
  /**
   * Renders the location component.
   * If the coordinates are provided to this component,
   * the coordinates will be rendered with external link to Google Maps,
   * otherwise the "Lokace neznámá" text will be rendered.
   * @returns {string}
   */
  render() {
    return !this.props._coords || !this.props._coords.latitude || !this.props._coords.longitude ? "Lokace neznámá" : (
      <a href={`https://www.google.com/maps/search/?api=1&query=${this.props._coords.latitude},${this.props._coords.longitude}`}
         target="_blank"
         rel="noopener noreferrer"
         className="mapLink"
         title="Otevřít v mapách Google">šířka: {parseFloat(this.props._coords.latitude).toFixed(5)}°, výška: {parseFloat(this.props._coords.longitude).toFixed(5)}° <img
        src="/img/google-maps.png" alt="google-maps-icon" className="gmIcon"/> <span className="icon extLink"></span></a>
    );
  }
}

export default Location;
