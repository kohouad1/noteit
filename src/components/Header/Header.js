import React, {Component} from 'react';
import "./Header.css";

/**
 * Represents the page header containing the application logo and current connection state.
 */
class Header extends Component {

  /**
   * Gets SVG elements by their ID and appends event listeners to them.
   */
  componentDidMount() {
    this.noteItIconSVG = document.querySelector("#noteItIconSVG");
    this.pencilPart1 = document.querySelector("#pencilPart1");
    this.pencilPart2 = document.querySelector("#pencilPart2");
    this.noteItIconSVG.addEventListener("mouseenter", () => {
      this.pencilPart1.classList.add("pencilPart1Hovered");
      this.pencilPart2.classList.add("pencilPart2Hovered");
    });
    this.noteItIconSVG.addEventListener("mouseleave", () => {
      this.pencilPart1.classList.remove("pencilPart1Hovered");
      this.pencilPart2.classList.remove("pencilPart2Hovered");
    });
  }

  /**
   * Renders the header.
   * @returns {*}
   */
  render() {
    return (
      <header id="header">
        <section>
          <h1><a href=".">
            <svg id="noteItIconSVG" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 123 128">
              <path id="pagePart"
                    d="M71.9,62v6.9a3,3,0,0,1-3,3H27.56a3,3,0,0,1-3-3V62a3,3,0,0,1,3-3H68.94A3,3,0,0,1,71.9,62Zm-3,17.73H27.56a3,3,0,0,0-3,3v6.9a3,3,0,0,0,3,3H68.94a3,3,0,0,0,3-3v-6.9A3,3,0,0,0,68.94,79.72Zm26.6-46.33v81.8A11.83,11.83,0,0,1,83.72,127H12.78A11.83,11.83,0,0,1,1,115.19V12.73A11.83,11.83,0,0,1,12.78.9H63.05A11.82,11.82,0,0,1,71.4,4.38L92.07,25A11.78,11.78,0,0,1,95.54,33.39ZM64,13.69V32.43H82.76Zm19.7,101.5V44.25H58.1a5.9,5.9,0,0,1-5.91-5.91V12.73H12.78V115.19H83.72Z"/>
              <path id="pencilPart1" d="M101.77,50l22,0.15-0.26,59.64-8.8,13.54a2.59,2.59,0,0,1-4.56,0l-8.68-13.67Z"/>
              <path id="pencilPart2"
                    d="M118.08,25.82l-10.35-.07c-3.23,0-5.86,3.22-5.88,7.25l-0.05,12.13,22,0.15,0.05-12.13C123.91,29.12,121.31,25.84,118.08,25.82Z"/>
            </svg>NoteIt</a></h1>
        </section>
        <div id={"infoSection"}>
          Stav připojení: {this.props.online ? <span className="green">online</span> : <span className="red">offline</span>}
        </div>
      </header>
    );
  }
}

export default Header;
