import React, {Component} from 'react';
import Header from "./components/Header/Header";
import Main from './components/Main/Main';
import CreateNoteModal from "./components/Modal/CreateNoteModal";
import DeleteNoteModal from "./components/Modal/DeleteNoteModal";
import DeleteAllNotesModal from "./components/Modal/DeleteAllNotesModal";
import CreateNoteFromFileModal from "./components/Modal/CreateNoteFromFileModal";
import EditNoteModal from "./components/Modal/EditNoteModal";

/**
 * Represents the main App component.
 * It also holds the main state of the application which includes
 * the array of saved notes, last received location coordinates and connection status.
 */
class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      _notes: [],
      _coords: { latitude: null, longitude: null },
      _online: navigator.onLine
    };
    this.clickSoundRef = React.createRef();
    this.slideSoundRef = React.createRef();
    this.editNoteModalRef = React.createRef();
    this.messageBox = React.createRef();
    this.message = React.createRef();
    this.initialPathname = window.location.pathname;
  }

  /**
   * Reads all notes from localStorage and saves them to current application state.
   * Receives current location coordinates and saves them to current application state.
   * Appends listeners to window for online/offline events.
   */
  componentDidMount() {
    this.clickSoundRef = document.querySelector("#clickSound");
    this.slideSoundRef = document.querySelector("#slideSound");
    const notes = localStorage.getItem("notes");
    if (notes) {
      let parsedNotes = JSON.parse(notes);
      for (let i = parsedNotes.length - 1; i >= 0; i--)
        this.addNote(parsedNotes[i]);
    }
    this.updateLocation();
    window.addEventListener("online", () => {this.setState({
      _online: navigator.onLine
    })});
    window.addEventListener("offline", () => {this.setState({
      _online: navigator.onLine
    })});
  }

  /**
   * Tries to receive current location.
   */
  updateLocation() {
    if (navigator.geolocation)
      navigator.geolocation.getCurrentPosition(this.updateCoords.bind(this), this.updateCoordsError.bind(this), {timeout: 10000});
  }

  /**
   * Updates the application state with newly received coordinates.
   * @param position object containing newly received coordinates
   */
  updateCoords(position) {
    this.setState({
      _coords: { latitude: position.coords.latitude, longitude: position.coords.longitude }
    });
    document.querySelector("#spinner").classList.remove("show");
    this.showMessage("Poloha úspěšně aktualizována.");
  }

  /**
   * Gets called when an error during location receiving happens.
   * @param error error object
   */
  updateCoordsError(error) {
    document.querySelector("#spinner").classList.remove("show");
    switch (error.code) {
      case 1:
        this.showMessage("Nelze získat polohu. Získání polohy nebylo povoleno."); break;
      case 2:
        this.showMessage("Nelze získat polohu. Zkontrolujte připojení k Internetu."); break;
      case 3:
        this.showMessage("Nelze získat polohu. Časový limit vypršel. Zkontrolujte připojení k Internetu a zkuste restartovat prohlížeč."); break;
      default:
        this.showMessage("Nelze získat polohu. Chybová zpráva: " + error.code);
    }
  }

  /**
   * Shows message at the bottom of the viewport.
   * @param messageText message to show
   */
  showMessage(messageText) {
    this.message.innerText = messageText;
    this.messageBox.classList.add("show");
    setTimeout(() => {this.messageBox.classList.remove("show")}, 5000);
  }

  /**
   * Returns an array of notes currently stored in application state.
   * @returns {Array} array of notes
   */
  getNotes() {
    return this.state._notes;
  }

  /**
   * Adds a new note to current application state and stores it to localStorage.
   * @param note a note to add
   */
  addNote(note) {
    this.state._notes.unshift(note);
    this.setState({
      _notes: this.state._notes
    });
    localStorage.setItem("notes", JSON.stringify(this.state._notes));
  }

  /**
   * Edits a note, updates current application state and the localStorage.
   * @param note a note to edit
   */
  editNote(note) {
    for (let i = 0; i < this.state._notes.length; i++) {
      if (this.state._notes[i]._id === note._id) {
        let updatedState = this.state._notes[i];
        updatedState._name = note._name;
        updatedState._text = note._text;
        updatedState._color = note._color;
        this.setState({
          _notes: this.state._notes
        });
        localStorage.setItem("notes", JSON.stringify(this.state._notes));
        break;
      }
    }
  }

  /**
   * Removes a note from current application state and localStorage.
   * @param noteID an ID of the note to remove
   */
  deleteNote(noteID) {
    for (let i = 0; i < this.state._notes.length; i++) {
      if (this.state._notes[i]._id === noteID) {
        this.state._notes.splice(i, 1);
        break;
      }
    }
    this.setState({
      _notes: this.state._notes
    });
    localStorage.setItem("notes", JSON.stringify(this.state._notes));
  }

  /**
   * Removes all notes from current application state and localStorage.
   */
  deleteAllNotes() {
    this.setState({
      _notes: []
    });
    localStorage.setItem("notes", "");
  }

  /**
   * Checks whether a note with a given ID exists in current application state.
   * @param noteID an ID of the note to look for
   * @returns {boolean} <code>true</code> if the note with the ID exists; <code>false</code> otherwise
   */
  exists(noteID) {
    for (let i = 0; i < this.state._notes.length; i++)
      if (this.state._notes[i]._id === noteID)
        return true;
    return false;
  }

  /**
   * Renders the most high-level components and modals.
   * @returns {*[]}
   */
  render() {
    return [
      <Header key={"headerKey"} online={this.state._online} coords={this.state._coords} />,
      <Main key={"mainKey"} app={this} />,
      <CreateNoteModal key={"createNoteModalKey"} app={this} />,
      <CreateNoteFromFileModal key={"createNoteFromFileModalKey"} app={this} />,
      <EditNoteModal ref={el => this.editNoteModalRef = el} key={"editNoteModalKey"} app={this} />,
      <DeleteNoteModal key={"deleteNoteModalKey"} app={this} />,
      <DeleteAllNotesModal key={"deleteAllNotesModalKey"} app={this} />,
      <div ref={el => this.messageBox = el} key={"messageBoxKey"} id="messageBox"><span ref={el => this.message = el} id="message"></span></div>
    ];
  }
}

export default App;
