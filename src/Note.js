import COLORS from "./Colors";

/**
 * Represents the note containing all the data about it.
 */
class Note {
  constructor(id, name, text, color = COLORS.YELLOW, date = new Date(), coords = { latitude: 50.089, longitude: 50.980 }) {
    this._id = id;
    this._name = name;
    this._text = text;
    this._color = color;
    this._date = date;
    this._coords = coords;
  }

  getID() {
    return this._ID;
  }

  setID(ID) {
    this._ID = ID;
  }

  getName() {
    return this._name;
  }

  setName(name) {
    this._name = name;
  }

  getText() {
    return this._text;
  }

  setText(text) {
    this._text = text;
  }
}

export default Note;
