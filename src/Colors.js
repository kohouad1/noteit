/**
 * Predefined colors and their hex values and names.
 * @type {{RED: {name: string, value: string}, GRAY: {name: string, value: string}, VIOLET: {name: string, value: string}, BLUE: {name: string, value: string}, YELLOW: {name: string, value: string}, GREEN: {name: string, value: string}, ORANGE: {name: string, value: string}}}
 */
const COLORS = {
  RED: {value: "#F1948A", name: "Červená"},
  VIOLET: {value: "#C39BD3", name: "Fialová"},
  BLUE: {value: "#85C1E9", name: "Modrá"},
  GREEN: {value: "#7DCEA0", name: "Zelená"},
  YELLOW: {value: "#F7DC6F", name: "Žlutá"},
  ORANGE: {value: "#F0B27A", name: "Oranžová"},
  GRAY: {value: "#D7DBDD", name: "Šedá"},
};

/**
 * Helper function that returns a color object by its provided corresponding hex value.
 * @param value a hex value of a color
 * @returns {COLORS.BLUE|{name, value}|COLORS.YELLOW|{name, value}|COLORS.GRAY|{name, value}|COLORS.RED|{name, value}|COLORS.ORANGE|{name, value}|COLORS.GREEN|{name, value}|COLORS.VIOLET|{name, value}}
 */
export function getColorTypeFromValue(value) {
  switch (value) {
    case "#F1948A":
      return COLORS.RED;
    case "#C39BD3":
      return COLORS.VIOLET;
    case "#85C1E9":
      return COLORS.BLUE;
    case "#7DCEA0":
      return COLORS.GREEN;
    case "#F7DC6F":
      return COLORS.YELLOW;
    case "#F0B27A":
      return COLORS.ORANGE;
    case "#D7DBDD":
      return COLORS.GRAY;
    default:
      return COLORS.YELLOW;
  }
}

export default COLORS;
