import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from "./App"
import * as serviceWorker from './serviceWorker';

// Starting point of the React application where the main App component is rendered
// to the root HTML element.
ReactDOM.render(<App />, document.querySelector("#root"));

serviceWorker.register();
