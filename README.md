# NoteIt
Repozitář zdrojového kódu pro webovou aplikaci umožňující jednoduché vytváření a správu poznámek.

## URL aplikace
Aplikace běží na adrese https://note-it-application.herokuapp.com/.

## Dokumentace
Kromě komentářů v JS souborech je ve složce *docs* i PDF dokumentace tohoto projektu.
